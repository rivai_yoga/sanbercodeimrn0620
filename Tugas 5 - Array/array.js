//Soal 1
function range(startNum, finishNum) {
	var array=[];
	if(startNum<finishNum){
		
		for(var a=startNum;a<=finishNum;a++){
			array.push(a);
		}
	}else if(startNum>finishNum){
		for(var a=startNum;a>=finishNum;a--){
			array.push(a);
		}
	}else{
		array.push(-1);
	}
	return array;
} 
console.log(range(5,1));

//Soal 2
function rangeWithStep(startNum, finishNum, step) {
	var array=[];
	if(startNum<finishNum){
		for(var a=startNum;a<=finishNum;a++){
			if( a % step == 1){
				array.push(a);
			}
		}
	}else if(startNum>finishNum){
		for(var a=startNum;a>=finishNum;a--){
			if( a % step == 1){
				array.push(a);
			}
		}
	}
	return array;
} 
console.log(rangeWithStep(10,1,3));

//Soal 3
function sum(a,b,c){
	var array=[];
	var hasil=0;
		if(a<b){
			for(var x=a;x<=b;x++){
				if(c!=null){
					if( x % c == 1){
						hasil=hasil+x;
					}
				}else{
					hasil=hasil+x;
				}
			}
		}else if(a>b){
			for(var x=a;x>=b;x--){
				if(c!=null){
					if( x % c == 1){
						hasil=hasil+x;
					}
				}else{
					hasil=hasil+x;
				}
			}
		}else{
			if(a!=null && b==null && c==null){
				hasil=1;
			}else{
				hasil=0;
			}
		}
		
	array.push(hasil);
	return array;
}
console.log(sum(1));

//Soal 4
var input = [
                ["0001","Roman Alamsyah","Bandar Lampung","21/05/1989","Membaca"],
                ["0002","Dika Sembiring","Medan","10/10/1992","Bermain Gitar"],
                ["0003","Winona", "Ambon","25/12/1965","Memasak"],
                ["0004","Bintang Senjaya","Martapura","6/4/1970","Berkebun"],
            ];
function dataHandling(input){
	var z=input.length-1;
	for(var a=0;a<=z;a++){
		console.log("Nomor ID :"+input[a][0]);
		console.log("Nama Lengkap :"+input[a][1]);
		console.log("TTL :"+input[a][2]);
		console.log("Hobi :"+input[a][3]);
		console.log("\n");
	}return ''
}
console.log(dataHandling(input));

//Soal 5
function balikKata(str){
	var a=str;
	var b='';
	for(var i=a.length-1;i>=0;i--){
		b=b+a[i];
	}return b;
}
console.log(balikKata("apapun"));

//Soal 6
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(a){
	a.splice(1,1,"Roman Alamsyah Elsharawy");
	a.splice(2,1,"Provinsi Bandar Lampung");
	a.splice(4,1,"Pria");
	a.splice(5,0,"SMA Internasional Metro");
	console.log(a);
	var split=a[3].split("/");
	var bulan = split[1];
	var nmbln;
	switch(bulan){
	case '01' :{nmbln="Januari";break;}
	case '02' :{nmbln="Februari";break;}
	case '03' :{nmbln="Maret";break;}
	case '04' :{nmbln="April";break;}
	case '05' :{nmbln="Mei";break;}
	case '06' :{nmbln="Juni";break;}
	case '07' :{nmbln="Juli";break;}
	case '08' :{nmbln="Agustus";break;}
	case '09' :{nmbln="September";break;}
	case 10 :{nmbln="Okrober";break;}
	case 11 :{nmbln="November";break;}
	case 12 :{nmbln="Desember";break;}
	
	}console.log(nmbln)
	console.log(split)
	var join = split.join("-");
	console.log(join)
	var slice=a[1];
	var iris = slice.slice(0,14)
	return iris;
}

console.log(dataHandling2(input));