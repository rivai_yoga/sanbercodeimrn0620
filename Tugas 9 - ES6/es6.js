//Soal 1
var golden = () => {
    console.log("this is golden!!")
} 
golden()

//Soal 2
var newFunction=(firstName,lastName)=>{
	console.log(firstName + " " + lastName);
}
newFunction("William", "Imoh");

//Soal 3
let newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation, spell }=newObject;
console.log(firstName, lastName, destination, occupation);

//Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
console.log(combined);

//Soal 5
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet,
 consectetur adipiscing elit,${planet} do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. 
	Ut enim ad minim veniam`
// Driver Code
console.log(before) 