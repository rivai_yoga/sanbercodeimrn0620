import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';

import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';

const Stack = createStackNavigator();
//Quiz
const loginStack=createStackNavigator();
const LoginStackScreen=()=>(
  <loginStack.Navigator>
    <loginStack.Screen name='Form Login' component={LoginScreen}/>
    <loginStack.Screen name='Barang' component={HomeScreen}/>
  </loginStack.Navigator>
)

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home" >
          <Stack.Screen name='Login' component={LoginStackScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
