import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator } from 'react-native';

export default class App extends Component {
	state={
		data:[]
	};
	
	componentWillMount(){
		this.fetchData();
	}
	
	fetchData=async()=>{
		const response = await fetch('https://sanberapp-a836d.firebaseio.com/Account.json');
		const json = await response.json();
		this.setState({data: json.items});
	}
	
	render(){
	  return(
		  <View style={styles.container}>
			<FlatList data={this.state.data}
			keyExtractor={(x,i)=>i}
			renderItem={({item})=>
			<Text style={styles.container}>
			{`${item.category}`}
			</Text>}/>
		  </View>
	)
  }
}

const styles = StyleSheet.create({
  container:{
	  flex:1,
	  justifyContent:"center",
	  alignItems:"center",
	  backgroundColor:"red"
  }
})