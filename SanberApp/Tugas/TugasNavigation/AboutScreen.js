import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  StatusBar
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Fa from 'react-native-vector-icons/AntDesign';

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const aboutScreen = ({ navigation }) => {
  return (
  <ScreenContainer>
    <ScrollView>
	  <View style={styles.container}>
	  <StatusBar 
    backgroundColor = {"grey"}
    translucent = {false}/>
	  	<View style={styles.header}>
	  		<Image source={require('./logo.png')} style={{ width: 360, height: 55 }} />
	  	</View>
	  	<View style={styles.cheader}><Text style={{fontWeight:'bold'}}>Tentang Saya</Text></View>
	  	<View style={styles.content}>
	  		<Icon style={styles.people} name="account-circle" size={100} />
	  		<Text style={styles.identity}>Rivai Yoga</Text>
        <Text style={styles.identity3}>Web And React Native Developer</Text>
        <View style={styles.box}>
          <Text style={styles.identity2}>Fortofolio</Text>
          <Text style={{borderWidth:1,height:1,width:360,borderColor:'white',marginTop:10}}></Text>
          <View style={styles.page}>
            <View style={styles.satu}>
              <Fa name="gitlab" style={{padding:5,textAlign:'center'}} size={50} />
              <Text style={styles.identity3}>@rivai_yoga</Text>
            </View>
            <View style={styles.satu}>
              <Fa name="github" style={{padding:5,textAlign:'center'}} size={50} />
              <Text style={styles.identity3}>@fake</Text>
            </View>
          </View>
        </View>
        <View style={styles.box}>
          <Text style={styles.identity2}>Tentang Saya</Text>
          <Text style={{borderWidth:1,width:360,height:1,borderColor:'white',marginTop:10}}></Text>
          <View style={styles.page}>
            <View style={styles.satu}>
              <Fa name="facebook-square" style={{padding:5,textAlign:'center'}} size={50} />
              <Text style={styles.identity3}>@rivai_yoga</Text>
            </View>
            <View style={styles.satu}>
              <Fa name="twitter" style={{padding:5,textAlign:'center'}} size={50} />
              <Text style={styles.identity3}>@rivai_yoga</Text>
            </View>
          </View>
        </View>
	  	</View>
	  	<View style={styles.footer}>
	  		<Text style={{fontWeight:'bold'}}>Copyright By @Student</Text>
	  	</View>
	  </View>
    </ScrollView>
    </ScreenContainer>
	)
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
  },
  header: {
  	marginTop:0,
  	height: 100,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center'
  },
  cheader:{
  	backgroundColor:'#57bce7',
  	flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height:55,
    width:400,
  },
  content:{
  	height:600,
  	padding:20
  },
  box:{
    marginTop:20,
    backgroundColor:'#57bce7',
    height:180,
    width:360,
    borderRadius:10,

  },
  people:{
    textAlign:'center',
    height:100,
    width:360
  },
  identity:{
    textAlign:'center',
    marginTop:20,
    color:'grey',
    color:'#036'
  },
  identity2:{
    textAlign:'center',
    marginTop:10,
    marginLeft:10,
    color:'grey',
    color:'#036'
  },
  identity3:{
    textAlign:'center',
    marginTop:10,
    marginLeft:10,
    color:'grey',
    color:'#036'
  },
  page:{
    width:360,
    height:150,
    padding:10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  satu:{
    width:170,
    height:150,
    padding:20,
    textAlign:'center'
  },
  footer:{
  	marginTop:50,
  	height: 60,
    backgroundColor: '#57bce7',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  }
});