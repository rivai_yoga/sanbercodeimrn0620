import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  SafeAreaView,
  StatusBar
} from 'react-native';

import VideoItem from './skill';
import data from './skillData.json';

import Icon from 'react-native-vector-icons/MaterialIcons';

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);
export const skillScreen = ({ navigation }) => (
  <ScreenContainer>
	 <SafeAreaView style={{flex: 1}}>
	  <View style={styles.container}>
	  <StatusBar 
    backgroundColor = {"grey"}
    translucent = {false}/>
	  	<View style={styles.header}>
	  		<Image source={require('../Tugas13/logo.png')} style={{ width: 180, height: 55 }} />
	  	</View>
	  	<View style={styles.account}>
	  		<Icon style={styles.people} name="account-circle" size={40} />
	  		<Text style={{marginTop:-20}}>Hai,</Text>
	  		<Text style={{marginLeft:-25,marginTop:10,fontWeight:'bold',color:'#036'}}>Rivai Yoga Aditama</Text>
	  	</View>
	  	<View style={styles.cheader}><Text style={{fontWeight:'bold',fontSize:30}}>Skill</Text></View>
	  	<View style={styles.border}></View>
      <View style={styles.submenu}>
        <View style={styles.menu}><Text style={{fontWeight:'bold',fontSize:10,textAlign:'center',marginTop:-3}}>Library / Framework</Text></View>
        <View style={styles.menu}><Text style={{fontWeight:'bold',fontSize:10,textAlign:'center',marginTop:-3}}>Bahasa Pemrograman</Text></View>
        <View style={styles.menu}><Text style={{fontWeight:'bold',fontSize:10,textAlign:'center',marginTop:-3}}>Teknologi</Text></View>
      </View>
	  	<View style={styles.content}>
        <FlatList
        data={data.items}
        renderItem={(skill)=><VideoItem skill={skill.item} />}
        keyExtractor={(skill)=>skill.id.toString()}
        ItemSeparatorComponent={()=><View style={{height:5,backgroundColor:'transparent'}}/>}
        />
	  	</View>
	  	<View style={styles.footer}>
	  		<Text style={{fontWeight:'bold'}}>Copyright By @Student</Text>
	  	</View>
	  </View>
	  </SafeAreaView>
  </ScreenContainer>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
  },
  header: {
  	marginLeft:200,
  	height: 60,
    elevation: 3,
    flexDirection: 'row',
    alignItems: 'center'
  },
  account:{
  	marginLeft:20,
  	height: 60,
    elevation: 3,
    flexDirection: 'row',
    alignItems: 'center',
  },
  cheader:{
  	marginLeft:30,
  	flexDirection: 'row',
    textAlign:'left',
    width:320,
  },
  border:{
  	marginLeft:30,
    height:1,
    width:325,
    borderWidth:2,
    borderColor:'#B4E9FF'
  },
  submenu:{
    marginTop:5,
    width:320,
    marginLeft:30,
    flexDirection: 'row',
  },
  menu:{
    backgroundColor:'#B4E9FF',
    padding:10,
    borderRadius:10,
    width:107,
    marginRight:2,
    marginBottom:10
  },
  content:{
  	height:440,
    width:325,
    marginLeft:30,
  },
  textBox:{
  	width:300,
  	height:40,
  	marginLeft:25,
  	borderWidth:1,
  	borderStyle:'solid',
  	borderColor:'grey',
  	marginBottom:20
  },
  button:{
    borderWidth:1,
    width:100,
    height:50,
    borderRadius:20,
    backgroundColor:'#036'
  },
  buttonArr:{
  	width:300,
  	flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft:25
  },
  text:{
  	textAlign:'center',
  	marginTop:13,
  	color:'white',
  	fontWeight:'bold'
  },
  footer:{
  	marginTop:50,
  	height: 60,
    backgroundColor: '#57bce7',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  }
});