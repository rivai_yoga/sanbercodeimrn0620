import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import {
  SignIn,
  CreateAccount,
  Profile,
  Home,
  Details
} from "./Screens";

import {loginScreen} from "./LoginScreen";
import {aboutScreen} from "./AboutScreen";
import {skillScreen} from "./skillScreen";

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const loginStack=createStackNavigator();
const ProfileStack=createStackNavigator();
const MenuStack=createStackNavigator();
const homeStack=createStackNavigator();
const SkillStack=createStackNavigator();

//Login
const LoginStackScreen=()=>(
  <loginStack.Navigator>
    <loginStack.Screen name='Form Login' component={loginScreen}/>
    <loginStack.Screen name='Main Menu' component={TabScreen}/>
  </loginStack.Navigator>
)

//Profile
const ProfileStackScreen=()=>(
  <ProfileStack.Navigator>
    <ProfileStack.Screen name='About Me' component={aboutScreen}/>
  </ProfileStack.Navigator>
)

//Skill
const SkillStackScreen=()=>(
  <SkillStack.Navigator>
    <SkillStack.Screen name='Skill' component={skillScreen}/>
  </SkillStack.Navigator>
)

//Menu
const MenuStackScreen=()=>(
  <MenuStack.Navigator>
    <MenuStack.Screen name='Main Menu' component={Home}/>
    <MenuStack.Screen name='Details' component={Details}/>
  </MenuStack.Navigator>
)

//Tabs
const TabScreen=()=>(
  <Tabs.Navigator>
      <Tabs.Screen name='Skill' component={SkillStackScreen}/>
      <Tabs.Screen name='Profile' component={ProfileStackScreen}/>
      <Tabs.Screen name='Tambah' component={MenuStackScreen}/>

    </Tabs.Navigator>
)

//drawer
const drawer = createDrawerNavigator();
export default () => (
  <NavigationContainer>
    <drawer.Navigator>
      <drawer.Screen name='loginStackScreen' component={LoginStackScreen}/>
      <drawer.Screen name='TabScreen' component={TabScreen}/>
    </drawer.Navigator>
    {/*<AuthStack.Navigator>
      <AuthStack.Screen
        name="SignIn" options={{title : 'Login'}}
        component={SignIn}
      />
      <AuthStack.Screen
        name="CreateAccount"
        component={CreateAccount}
        options={{title : 'Create Account'}}
      />
    </AuthStack.Navigator>*/}
  </NavigationContainer>
);

