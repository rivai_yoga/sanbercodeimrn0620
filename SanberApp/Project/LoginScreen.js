import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  StatusBar,
  TextInput,
  Button
} from 'react-native';

export default class App extends Component {
	constructor(props) {
		super(props)
		this.state = {
		  username: '',
		  password: '',
		  isError: false,
		}
	}
	changeScreen=(a)=>{
		let aa=this.state.username
		this.setState({username:aa});
		let bb=this.state.password
		this.setState({password:bb});
		if(this.state.username==='hplstriker' && this.state.password==='coba' && a==='Home'){
			this.setState({isError:false});
			this.props.navigation.navigate(a,{ name: this.state.username });
		}else if(a==='Register'){
			this.props.navigation.navigate(a);
		}else{
			this.setState({isError:true});
			alert('Username atau Password Yang Anda Masukkan Salah !!');
		}
	}
	render(){
	  return(
	  <ScrollView>
	  <View style={styles.container}>
	  <StatusBar 
    backgroundColor = {"grey"}
    translucent = {false}/>
	  	<View style={styles.header}>
	  		<Image source={require('./header.png')} style={{ width: 400, height: 100 }} />
	  	</View>
	  	<View style={styles.cheader}></View>
	  	<View style={styles.content}>
			<Text style={{textAlign:'center',fontWeight:'bold',fontSize:24,marginBottom:5}}>FORM LOGIN</Text>
			<View style={{width:340,borderColor:'black',borderWidth:1,borderStyle:'solid',marginBottom:15}}></View>
	  		<Text style={styles.textWord}>Username / Email :</Text>
	  		<TextInput
                style={styles.textInput}
                placeholder=''
                onChangeText={username => this.setState({ username })}
            />
	  		<Text style={styles.textWord}>Password :</Text>
	  		<TextInput
                style={styles.textInput}
                placeholder=''
                onChangeText={password => this.setState({ password })}
				secureTextEntry={true}
            />
	  		
	  		<View style={styles.buttonArr}>
	  			<Button title='Masuk' style={styles.button} onPress={() => this.changeScreen('Home')} />
	  		</View>
	  		<View style={styles.buttonArr}>
	  			<Text style={{marginTop:10,marginBottom:10}}>Atau</Text>
	  		</View>
	  		<View style={styles.buttonArr}>
	  			<Button title='Daftar' style={styles.button} onPress={() => this.changeScreen('Register')} />
	  		</View>
	  	</View>
	  </View>
	  </ScrollView>
	  )
	}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
  	marginTop:0,
  	height: 100,
    backgroundColor: 'white',
    elevation: 3,
    flexDirection: 'row',
    alignItems: 'center'
  },
  cheader:{
  	backgroundColor:'#f58634',
  	flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height:30,
    width:400,
  },
  content:{
  	padding:30,
	height:645,
	justifyContent: 'center',
	backgroundColor:'#FFF7F1'
  },
  textWord:{
	textAlign: 'left',
	fontWeight:'bold'
  },
  textInput:{
	alignItems: 'center',
  	width:340,
	padding:10,
  	height:40,
  	borderWidth:1,
  	borderStyle:'solid',
  	borderColor:'#cec7c7',
	backgroundColor:'white',
  	marginBottom:20
  },
  button:{
    borderWidth:1,
    width:100,
    height:30,
    borderRadius:20,
    backgroundColor:'red'
  },
  buttonArr:{
  	width:300,
  	flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft:25
  },
  text:{
  	textAlign:'center',
  	marginTop:13,
  	color:'white',
  	fontWeight:'bold'
  }
});