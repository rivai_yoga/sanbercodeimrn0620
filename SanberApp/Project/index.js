import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import 'react-native-gesture-handler';

import loginScreen from "./LoginScreen";
import registerScreen from "./registerScreen";
import aboutScreen from "./AboutScreen";
import listScreen from "./listScreen";
import detailScreen from "./Main";

//Front
const frontStack=createStackNavigator();
const frontStackScreen=()=>(
  <frontStack.Navigator>
    <frontStack.Screen name='Login' component={loginScreen} options={{ headerShown: false }}/>
    <frontStack.Screen name='Register' component={registerScreen} options={{ headerShown: false }}/>
  </frontStack.Navigator>
)

//Profile
const ProfileStack=createStackNavigator();
const ProfileStackScreen=()=>(
  <ProfileStack.Navigator>
    <ProfileStack.Screen name='About Me' component={aboutScreen}/>
  </ProfileStack.Navigator>
)

//List
const ListStack=createStackNavigator();
const ListStackScreen=()=>(
  <ListStack.Navigator>
    <ListStack.Screen name='Kamar' component={listScreen} options={{ headerShown: false }}/>
    <ListStack.Screen name='Detail' component={detailScreen}/>
  </ListStack.Navigator>
)


//Tabs
const Tabs = createBottomTabNavigator();
const HomeScreen=()=>(
  <Tabs.Navigator>
      <Tabs.Screen name='Kamar' component={ListStackScreen} />
      <Tabs.Screen name='Profile' component={ProfileStackScreen}/>

    </Tabs.Navigator>
)

const Stack = createStackNavigator();
export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Front" >
          <Stack.Screen name='Front' component={frontStackScreen} options={{ headerShown: false }} />
          <Stack.Screen name='Home' component={HomeScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

