import React, { Component } from 'react';
import { Button, View, Text, FlatList, StyleSheet, Image, ActivityIndicator } from 'react-native';

export default class App extends Component {
	constructor(props) {
		super(props)
		this.state = {
		  data: [],
		  identitas: '',
		  isError: false,
		}
	  }
	
	componentWillMount(){
		this.fetchData();
	}
	
	fetchData=async()=>{
		const response = await fetch('https://sanberapp-a836d.firebaseio.com/Account.json');
		const json = await response.json();
		this.setState({data: json.items});
	}
	
	detailScreen=(a,b,c,d)=>{
		this.props.navigation.navigate(a,{ ruang: b,biaya:c,fasilitas:d });
	}
	render(){
	  return(
		  <View style={styles.container}>
			<View style={styles.content}>
				<FlatList data={this.state.data}
				keyExtractor={(x,i)=>i}
				style={{width:200}}
				renderItem={({item})=>
				<View style={styles.listContent}>
					<Button title={`${item.Ruang}`} style={styles.button} onPress={() => this.detailScreen('Detail',
					`${item.Ruang}`,`${item.Biaya}`,`${item.Fasilitas}`)} />
				</View>
				}
				/>
			</View>
		  </View>
	)
  }
}

const styles = StyleSheet.create({
  container:{
	flex: 1,
	flexDirection: 'column',
	justifyContent: 'space-between',
  },
  content:{
	margin:20  
  },
  listContent:{
	padding:10, 
	borderColor:'black',
	borderWidth:1,
	borderStyle:'solid',
	backgroundColor: 'powderblue',
	marginBottom:15
  }
})