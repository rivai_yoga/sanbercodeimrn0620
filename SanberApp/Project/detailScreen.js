import React, { Component } from 'react';
import { Button, View, Text, FlatList, StyleSheet, Image, ActivityIndicator } from 'react-native';

export default class App extends Component {
	constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      totalPrice: 0,
    }
  }
	render(){
	  return(
		  <View style={styles.container}>
			<View style={styles.listContent}>
				<Text>
					Nama Ruang : {this.props.route.params.ruang}
				</Text>
			</View>
			<View style={styles.listContent}>
				<Text>
					Fasilitas : {this.props.route.params.fasilitas}
				</Text>
			</View>
			<View style={styles.listContent}>
				<Text>
					Biaya Kamar : {this.props.route.params.biaya}
				</Text>
			</View>
			
		  </View>
	)
  }
}

const styles = StyleSheet.create({
  container:{
	flex: 1,
  },
  content:{
	margin:20  
  },
  listContent:{
	padding:10, 
	borderColor:'black',
	borderWidth:1,
	borderStyle:'solid',
	backgroundColor: 'powderblue',
	marginBottom:2
  }
})