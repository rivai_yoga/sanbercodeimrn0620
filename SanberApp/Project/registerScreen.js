import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  StatusBar,
  TextInput,
  Button
} from 'react-native';

export default class App extends Component {
	constructor(props) {
		super(props)
		this.state = {
		  username: '',
		  email: '',
		  no_hp: '',
		  password: '',
		  isError: false,
		}
	}
	
	changeScreen=(a)=>{
		this.props.navigation.navigate(a);
	}
	
	render(){
	  return(
	  <ScrollView>
	  <View style={styles.container}>
	  <StatusBar 
    backgroundColor = {"grey"}
    translucent = {false}/>
	  	<View style={styles.header}>
	  		<Image source={require('./header.png')} style={{ width: 400, height: 100 }} />
	  	</View>
	  	<View style={styles.cheader}></View>
	  	<View style={styles.content}>
			<Text style={{textAlign:'center',fontWeight:'bold',fontSize:24,marginBottom:5}}>FORM REGISTRASI</Text>
			<View style={{width:340,borderColor:'black',borderWidth:1,borderStyle:'solid',marginBottom:15}}></View>
	  		<Text style={styles.textWord}>Username :</Text>
	  		<TextInput
                style={styles.textInput}
                placeholder=''
                onChangeText={username => this.setState({ username })}
            />
	  		<Text style={styles.textWord}>Email :</Text>
	  		<TextInput
                style={styles.textInput}
                placeholder=''
                onChangeText={email => this.setState({ email })}
            />
	  		<Text style={styles.textWord}>No HP :</Text>
	  		<TextInput
                style={styles.textInput}
                placeholder=''
                onChangeText={no_hp => this.setState({ no_hp })}
            />
	  		<Text style={styles.textWord}>Password :</Text>
	  		<TextInput
                style={styles.textInput}
                placeholder=''
                onChangeText={password => this.setState({ password })}
				secureTextEntry={true}
            />
	  		
	  		<View style={styles.buttonArr}>
	  			<Button title='Daftar' style={styles.button} onPress={() => this.changeScreen('Login')} />
	  		</View>
	  		<View style={styles.buttonArr}>
	  			<Text style={{marginTop:10,marginBottom:10}}>Atau</Text>
	  		</View>
	  		<View style={styles.buttonArr}>
	  			<Button title='Masuk' color='#49c81b' onPress={() => this.changeScreen('Login')} />
	  		</View>
	  	</View>
	  </View>
	  </ScrollView>
	  )
	}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#FFF7F1',
  },
  header: {
  	marginTop:0,
  	height: 100,
    backgroundColor: 'white',
    elevation: 3,
    flexDirection: 'row',
    alignItems: 'center'
  },
  cheader:{
  	backgroundColor:'#f58634',
  	flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height:30,
    width:400,
  },
  content:{
  	padding:30,
	height:645,
	justifyContent: 'center'
  },
  textWord:{
	textAlign: 'left',
	fontWeight:'bold'
  },
  textInput:{
	alignItems: 'center',
  	width:340,
	padding:10,
  	height:40,
  	borderWidth:1,
  	borderStyle:'solid',
  	borderColor:'#cec7c7',
	backgroundColor:'white',
  	marginBottom:20
  },
  button:{
    borderWidth:1,
    width:100,
    height:30,
    borderRadius:20
  },
  buttonArr:{
  	width:300,
  	flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft:25
  },
  text:{
  	textAlign:'center',
  	marginTop:13,
  	color:'white',
  	fontWeight:'bold'
  },
  footer:{
  	marginTop:50,
  	height: 40,
    backgroundColor: '#f58634',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  }
});