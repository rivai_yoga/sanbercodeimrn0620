/**
 * Berikut soal quiz kali ini
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/* 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
 constructor(points) {
        this.subject;
        this.points=points;
        this.email;
    }
	a = () => {
		var no=0;
		for(let a=0;a<=this.points.length-1;a++){
			no=no+this.points[a];
		}
		let xx=no/this.points.length;
		console.log(xx);
	}
}
var a=[1,2,3,4,5];
var average = new Score(a);
average.a()

/* 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
	let xx={};
	for(let a=1;a<=data.length-1;a++){
		if(subject=='quiz-1'){var point=data[a][1]}
		else if(subject=='quiz-2'){var point=data[a][2]}
		else if(subject=='quiz-3'){var point=data[a][3]}
		xx[a] = {
		  email: data[a][0],
		  subject: subject,
		  points: point
		}
		const { email, points}=xx;
	}
	console.log(xx)
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/* 
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  recapScore
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
	let xxx={};
	for(let a=1;a<=data.length-1;a++){
		let average=(data[a][1]+data[a][2]+data[a][3])/3;
		if(average>80){var point="graduate";}
		else if(average>70){var point="participant";}
		else if(average>90){var point="honour"}
		xxx[a] = {
		  email: data[a][0],
		  rata_rata: average,
		  predikat: point
		}
		const { email, points}=xxx;
	}
	console.log(xxx);
}

recapScores(data);
